#!/usr/bin/env bash
# This shell script is PUBLIC DOMAIN. You may do whatever you want with it.


# a quick and dirty script to remap the arrow keys
# execute a 2nd time to undo


# if you change the keys you have to change the keycodes and ORIG_ACTION together
# else the restoring will fail

# done with these posts as help:
# http://askubuntu.com/questions/296155/how-can-i-remap-keyboard-keys
# http://askubuntu.com/questions/147080/shell-script-to-toggle-between-two-commands

# execute the "xev" command to see the keycodes
# for more details see first post

# TODO actually save and restore the original keyactions
# without having to manually configure those

TOGGLE=$HOME/.restore_keys

UP=25
LEFT=38
DOWN=39
RIGHT=40

ORIG_ACTION_UP="w"
ORIG_ACTION_LEFT="a"
ORIG_ACTION_DOWN="s"
ORIG_ACTION_RIGHT="d"


if [ ! -e $TOGGLE ]; then
    touch $TOGGLE

    xmodmap -e "keycode $UP    = Up"
    xmodmap -e "keycode $LEFT  = Left"
    xmodmap -e "keycode $DOWN  = Down"
    xmodmap -e "keycode $RIGHT = Right"
    echo "remapped keys"
else
    rm $TOGGLE

    xmodmap -e "keycode $UP    = $ORIG_ACTION_UP"
    xmodmap -e "keycode $LEFT  = $ORIG_ACTION_LEFT"
    xmodmap -e "keycode $DOWN  = $ORIG_ACTION_DOWN"
    xmodmap -e "keycode $RIGHT = $ORIG_ACTION_RIGHT"
    echo "keys restored"
fi


